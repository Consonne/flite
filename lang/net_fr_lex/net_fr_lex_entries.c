/*******************************************************/
/**  generated lexicon entry file from net_fr    */
/*******************************************************/

#include "cst_string.h"
#include "cst_lexicon.h"

const int net_fr_lex_num_bytes = 
#include "net_fr_lex_num_bytes.c"
;

const int net_fr_lex_num_entries = 8649;

const char * const net_fr_lex_phone_table[46] = 
{
    "_epsilon_",
    "a0",
    "l",
    "eh0",
    "t",
    "i0",
    "ohn0",
    "j",
    "s",
    "k",
    "d",
    "b",
    "ahn0",
    "m",
    "e0",
    "rh",
    "oh0",
    "p",
    "y",
    "n",
    "ae0",
    "w",
    "oe0",
    "z",
    "sh",
    "v",
    "f",
    "hw",
    "eu0",
    "ehn0",
    "g",
    "zh",
    "u0",
    "o0",
    "jg",
    "hh",
    "hs",
    "oen0",
    "ah0",
    "nj",
    "ad0",
    "ng",
    "hhh",
    "ehs0",
    "sm",
    NULL
};

const char * const net_fr_lex_phones_huff_table[257] = 
{
    NULL, /* reserved */
#include "net_fr_lex_phones_huff_table.c"
    NULL
};

const char * const net_fr_lex_entries_huff_table[257] = 
{
    NULL, /* reserved */
#include "net_fr_lex_entries_huff_table.c"
    NULL
};

