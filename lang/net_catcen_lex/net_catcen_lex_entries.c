/*******************************************************/
/**  generated lexicon entry file from net_catcen    */
/*******************************************************/

#include "cst_string.h"
#include "cst_lexicon.h"

const int net_catcen_lex_num_bytes = 
#include "net_catcen_lex_num_bytes.c"
;

const int net_catcen_lex_num_entries = 42339;

const char * const net_catcen_lex_phone_table[39] = 
{
    "_epsilon_",
    "a11",
    "E11",
    "b",
    "s",
    "d",
    "ax0",
    "f",
    "Z",
    "k",
    "i11",
    "t",
    "O11",
    "l",
    "m",
    "n",
    "e11",
    "p",
    "u11",
    "rr",
    "S",
    "o11",
    "g",
    "r",
    "z",
    "J",
    "u0",
    "i0",
    "w",
    "L",
    "a0",
    "O0",
    "j",
    "e0",
    "o0",
    "E0",
    "u1",
    "h",
    NULL
};

const char * const net_catcen_lex_phones_huff_table[257] = 
{
    NULL, /* reserved */
#include "net_catcen_lex_phones_huff_table.c"
    NULL
};

const char * const net_catcen_lex_entries_huff_table[257] = 
{
    NULL, /* reserved */
#include "net_catcen_lex_entries_huff_table.c"
    NULL
};

