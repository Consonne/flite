/*************************************************************************/
/*                                                                       */
/*                  Language Technologies Institute                      */
/*                     Carnegie Mellon University                        */
/*                        Copyright (c) 2001                             */
/*                        All Rights Reserved.                           */
/*                                                                       */
/*  Permission is hereby granted, free of charge, to use and distribute  */
/*  this software and its documentation without restriction, including   */
/*  without limitation the rights to use, copy, modify, merge, publish,  */
/*  distribute, sublicense, and/or sell copies of this work, and to      */
/*  permit persons to whom this work is furnished to do so, subject to   */
/*  the following conditions:                                            */
/*   1. The code must retain the above copyright notice, this list of    */
/*      conditions and the following disclaimer.                         */
/*   2. Any modifications must be clearly marked as such.                */
/*   3. Original authors' names are not deleted.                         */
/*   4. The authors' names are not used to endorse or promote products   */
/*      derived from this software without specific prior written        */
/*      permission.                                                      */
/*                                                                       */
/*  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         */
/*  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      */
/*  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   */
/*  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      */
/*  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    */
/*  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   */
/*  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          */
/*  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       */
/*  THIS SOFTWARE.                                                       */
/*                                                                       */
/*************************************************************************/
/*             Author:  Alan W Black (awb@cs.cmu.edu)                    */
/*               Date:  January 2001                                     */
/*************************************************************************/
/*                                                                       */
/*  CMU Lexicon definition                                               */
/*                                                                       */
/*************************************************************************/

#include "flite.h"

#include "net_catcen_lex.h"
#include "cst_lts_iso.h"
#include <signal.h>

extern const int net_catcen_lex_entry[];
extern const unsigned char net_catcen_lex_data[];
extern const int net_catcen_lex_num_entries;
extern const int net_catcen_lex_num_bytes;
extern const char * const net_catcen_lex_phone_table[86];
extern const char * const net_catcen_lex_phones_huff_table[];
extern const char * const net_catcen_lex_entries_huff_table[];

static int net_is_vowel(const char *p);
static int net_is_silence(const char *p);
static int net_has_vowel_in_list(const cst_val *v);
static int net_has_vowel_in_syl(const cst_item *i);
static int net_sonority(const char *p);

static const char * const addenda0[] = { "p,", NULL };
static const char * const addenda1[] = { "p.", NULL };
static const char * const addenda2[] = { "p(", NULL };
static const char * const addenda3[] = { "p)", NULL };
static const char * const addenda4[] = { "p[", NULL };
static const char * const addenda5[] = { "p]", NULL };
static const char * const addenda6[] = { "p{", NULL };
static const char * const addenda7[] = { "p}", NULL };
static const char * const addenda8[] = { "p:", NULL };
static const char * const addenda9[] = { "p;", NULL };
static const char * const addenda10[] = { "p?", NULL};
static const char * const addenda11[] = { "p!", NULL };
static const char * const addenda12[] = { "n@", "ae1", "t", NULL };
static const char * const addenda13[] = { "n#", "hh", "ae1","sh", NULL };
static const char * const addenda14[] = { "n$", "d", "aa1", "l", "er", NULL };
static const char * const addenda15[] = { "n%", "p", "er", "s", "eh1", "n", "t", NULL };
static const char * const addenda16[] = { "n^", "k", "eh1", "r", "eh1", "t",  NULL };
static const char * const addenda17[] = { "n&","ae1","m","p","er","s","ae1","n","d", NULL };
static const char * const addenda18[] = { "n*","ae1","s","t","er","ih1","s","k",NULL };
static const char * const addenda19[] = { "n|","b","aa1","r",NULL };
static const char * const addenda20[] = { "n\\","b","ae1","k","s","l","ae1","sh",NULL };
static const char * const addenda21[] = { "n=","iy1","k","w","ax","l","z",NULL};
static const char * const addenda22[] = { "n+","p","l","ah1","s",NULL};
static const char * const addenda23[] = { "n~","t","ih1","l","d","ax",NULL};
static const char * const addenda24[] = { "p'",NULL};
static const char * const addenda25[] = { "p`",NULL};
static const char * const addenda26[] = { "p\"",NULL};
static const char * const addenda27[] = { "p-",NULL};
static const char * const addenda28[] = { "p<",NULL};
static const char * const addenda29[] = { "p>",NULL};
static const char * const addenda30[] = { "n_","ah1","n","d","er","s","k","ao1","r",NULL};
static const char * const addenda35[] = { "n/","s","l","a1","sh",NULL};


static const char * const * const addenda[] = {
    addenda0,
    addenda1,
    addenda2,
    addenda3,
    addenda4,
    addenda5,
    addenda6,
    addenda7,
    addenda8,
    addenda9,
    addenda10,
    addenda11,
    addenda12,
    addenda13,
    addenda14,
    addenda15,
    addenda16,
    addenda17,
    addenda18,
    addenda19,
    addenda20,
    addenda21,
    addenda22,
    addenda23,
    addenda24,
    addenda25,
    addenda26,
    addenda27,
    addenda28,
    addenda29,
    addenda30,
    addenda35,
    NULL };

static int net_is_silence(const char *p)
{
    if (cst_streq(p,"pau"))
	return TRUE;
    else
	return FALSE;
}

static int net_has_vowel_in_list(const cst_val *v)
{
    const cst_val *t;

    for (t=v; t; t=val_cdr(t))
	if (net_is_vowel(val_string(val_car(t))))
	    return TRUE;
    return FALSE;
}

static int net_has_vowel_in_syl(const cst_item *i)
{
    const cst_item *n;

    for (n=i; n; n=item_prev(n))
	if (net_is_vowel(item_feat_string(n,"name")))
	    return TRUE;
    return FALSE;
}

static int net_is_vowel(const char *p)
{
    //  a E An On 9n  etc..
    if (strchr("aeiou92AEOy@",p[0]) == NULL)
	return FALSE;
    else
	return TRUE;
}

static int net_sonority(const char *p)
{
    /* A bunch of hacks for US English phoneset */
    if (net_is_vowel(p) || (net_is_silence(p)))
	return 5;
    else if (strchr("wylr",p[0]) != NULL)
	return 4;  /* glides/liquids */
    else if (strchr("nm",p[0]) != NULL)
	return 3;  /* nasals */
    else if (strchr("bdgjlmnnnrvwyz",p[0]) != NULL)
	return 2;  /* voiced obstruents */
    else
	return 1;
}

int net_catcen_syl_boundary(const cst_item *i,const cst_val *rest)
{
    /* Returns TRUE if this should be a syllable boundary */
    /* This is of course phone set dependent              */
    int p, n, nn;

    if (rest == NULL)
	return TRUE;
    else if (net_is_silence(val_string(val_car(rest))))
	return TRUE;
    else if (!net_has_vowel_in_list(rest)) /* no more vowels so rest *all* coda */
	return FALSE;
    else if (!net_has_vowel_in_syl(i))  /* need a vowel */
	return FALSE;
    else if (net_is_vowel(val_string(val_car(rest))))
	return TRUE;
    else if (val_cdr(rest) == NULL)
	return FALSE;
    else 
    {   /* so there is following vowel, and multiple phones left */
	p = net_sonority(item_feat_string(i,"name"));
	n = net_sonority(val_string(val_car(rest)));
	nn = net_sonority(val_string(val_car(val_cdr(rest))));

	if ((p <= n) && (n <= nn))
	    return TRUE;
	else
	    return FALSE;
    }
}

static int net_catcen_lex_dist_to_vowel(const cst_val *rest)
{
    if (rest == 0)
        return 0;  /* shouldn't get here */
    else if (net_is_vowel(val_string(val_car(rest))))
        return 0;
    else
        return 1+net_catcen_lex_dist_to_vowel(val_cdr(rest));
}

static const char * const net_catcen_lex_onset_trigrams[] = {
    "str", "spy", "spr", "spl", "sky", "skw", "skr", "skl", NULL
};
static const char * const net_catcen_lex_onset_bigrams[] = {
    "zw", "zl",
    "vy", "vr", "vl",
    "thw", "thr",
    "ty", "tw",
    "tr", /* "ts", */
    "shw", "shr", "shn", "shm", "shl",
    "sw", "sv", "st", "sr", "sp", "sn", "sm", "sl", "sk", "sf",
    "py", "pw", "pr", "pl",
    "ny",
    "my", "mr",
    "ly",
    "ky", "kw", "kr", "kl",
    "hhy", "hhw", "hhr", "hhl",
    "gy", "gw", "gr", "gl", 
    "fy", "fr", "fl", 
    "dy", "dw", "dr",
    "by", "bw", "br", "bl",
    NULL
};

static int net_catcen_lex_onset_bigram(const cst_val *rest)
{
    char x[10];
    int i;

    cst_sprintf(x,"%s%s",val_string(val_car(rest)),
           val_string(val_car(val_cdr(rest))));
    for (i=0; net_catcen_lex_onset_bigrams[i]; i++)
        if (cst_streq(x,net_catcen_lex_onset_bigrams[i]))
            return TRUE;
    return FALSE;
}

static int net_catcen_lex_onset_trigram(const cst_val *rest)
{
    char x[15];
    int i;

    cst_sprintf(x,"%s%s%s",val_string(val_car(rest)),
           val_string(val_car(val_cdr(rest))),
           val_string(val_car(val_cdr(val_cdr(rest)))));
    for (i=0; net_catcen_lex_onset_trigrams[i]; i++)
        if (cst_streq(x,net_catcen_lex_onset_trigrams[i]))
            return TRUE;
    return FALSE;
}

int net_catcen_syl_boundary_mo(const cst_item *i,const cst_val *rest)
{
    /* syl boundary maximal onset */
    int d2v;

    if (rest == NULL)
	return TRUE;
    else if (net_is_silence(val_string(val_car(rest))))
	return TRUE;
    else if (!net_has_vowel_in_list(rest)) 
        /* no more vowels so rest *all* coda */
	return FALSE;
    else if (!net_has_vowel_in_syl(i))  /* need a vowel */
        /* no vowel yet in syl so keep copying */
	return FALSE;
    else if (net_is_vowel(val_string(val_car(rest))))
        /* next is a vowel, syl has vowel, so this is a break */
	return TRUE;
    else if (cst_streq("ng",val_string(val_car(rest))))
        /* next is "ng" which can't start a word internal syl */
	return FALSE;
    else 
    {
        /* want to know if from rest to the next vowel is a valid onset */
        d2v = net_catcen_lex_dist_to_vowel(rest);
        if (d2v < 2)
            return TRUE;
        else if (d2v > 3)
            return FALSE;
        else if (d2v == 2) 
            return net_catcen_lex_onset_bigram(rest);
        else /* if (d2v == 3) */
            return net_catcen_lex_onset_trigram(rest);
        return TRUE;
    }

}

cst_lexicon net_catcen_lex;
cst_lts_rules net_catcen_lts_rules;
extern const char * const net_catcen_lts_phone_table[];
extern const char * const net_catcen_lts_letter_table[];
extern const cst_lts_addr net_catcen_lts_letter_index[];
extern const cst_lts_model net_catcen_lts_model[];


cst_val *net_catcen_lex_lts_function(const struct lexicon_struct *l, 
                                   const char *word, const char *pos)
{  
    cst_val *phones = 0;// = NULL;
     //uintptr_t address;
    if (cst_streq(cst_substr(word, 0, 3), "ph="))
    {
        printf("ph= \n");
        // cst_repchr(cst_substr(word, 3, cst_strlen(word)),'-',' ') 
        //phones = val_readlist_string(cst_substr(word, 3, cst_strlen(word) ));
        phones = val_readlist_string(cst_repchr(cst_substr(word, 3, cst_strlen(word)),'-',' '));
    }
    else
    {

    phones= lts_iso_apply(word,"",  /* more features if we had them */
l->lts_rule_set);
	//phones = (void *) address;
    }
printf("now here \n");
printf("%p\n", (void *) &phones);
printf("value %p\n", phones);
val_print(stdout,phones);
printf("here indeed \n");
//phones = address;
    return phones;
}

cst_lexicon *net_catcen_lex_init(void)
{
    /* I'd like to do this as a const but it needs everything in this */
    /* file and already the bits are too big for some compilers */
    
    if (net_catcen_lts_rules.name)
        return &net_catcen_lex;  /* Already initialized */

    net_catcen_lts_rules.name = "net";
    net_catcen_lts_rules.letter_index = net_catcen_lts_letter_index;

    net_catcen_lts_rules.models = net_catcen_lts_model;

    net_catcen_lts_rules.phone_table = net_catcen_lts_phone_table;
    net_catcen_lts_rules.context_window_size = 4;
    net_catcen_lts_rules.context_extra_feats = 1;
    net_catcen_lts_rules.letter_table = net_catcen_lts_letter_table;

    net_catcen_lex.name = "net";
    net_catcen_lex.num_entries = net_catcen_lex_num_entries;
    net_catcen_lex.lts_function = net_catcen_lex_lts_function;

    net_catcen_lex.data = (unsigned char *)(void *)net_catcen_lex_data;
    net_catcen_lex.num_bytes = net_catcen_lex_num_bytes;
    net_catcen_lex.phone_table = (char **) net_catcen_lex_phone_table;
    net_catcen_lex.syl_boundary = net_catcen_syl_boundary_mo;
    net_catcen_lex.addenda = (char ***) addenda;
    net_catcen_lex.lts_rule_set = (cst_lts_rules *) &net_catcen_lts_rules;

    net_catcen_lex.phone_hufftable = net_catcen_lex_phones_huff_table;
    net_catcen_lex.entry_hufftable = net_catcen_lex_entries_huff_table;

    net_catcen_lex.postlex = net_catcen_postlex;

    return &net_catcen_lex;

}



