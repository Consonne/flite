#ifndef _UTF8_TO_LATIN9_H__
#define _UTF8_TO_LATIN9__

//size_t utf8_to_latin9(char *const output, const char *const input, const size_t length);
char *utf8_to_latin9(const char *const string);

#endif
