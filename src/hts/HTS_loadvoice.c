#include "flite.h"
#include "flite_hts_engine.h"
#include <string.h>

/*************************************************************************/
/*             Author:  Consonne                                         */
/*               Date:  April 2010                                       */
/*************************************************************************/
/*                                                                       */
/*  Load a clustergen voice from a file                                  */
/*                                                                       */
/*************************************************************************/


//#include "../../lang/net_fr_lex/net_fr_lex.h"
//#include "../../lang/net_fr_lang/net_fr_lang.h"

cst_lexicon *hts_init_lang_lex(cst_voice *vox,
                              const cst_lang *lang_table,
                              const char *language)
{
    cst_lexicon *lex = NULL;
    int i;

    /* Search Lang table for lang_init() and lex_init(); */
    for (i=0; lang_table[i].lang; i++)
    {
        if (cst_streq(language,lang_table[i].lang))
        {
            (lang_table[i].lang_init)(vox);
            lex = (lang_table[i].lex_init)();
            break;
        }
    }

    /* NULL if lang/lex not found */
    return lex;
}

cst_voice *cst_hts_load_voice(const char *filename,
                             const cst_lang *lang_table, const char *name)
{
    cst_voice *vox;
    cst_lexicon *lex = NULL;
    int end_of_features;
    const char *language;
    const char *xname;
    cst_val *secondary_langs;
    const cst_val *sc;
    char* fname;
    char* fval;
    int r;
    int byteswapped = 0;

    Flite_HTS_Engine *flite_hts = NULL;
    printf("000000\n");
    vox = new_voice();



    /* Use the language feature to initialize the correct voice */
    language = "net_fr_lang"; //flite_get_param_string(vox->features, "language", "");
    printf("bbbbbbb\n");
    /* Some languages require initialization of more than one language */
    /* e.g. Indic languages require English initialization too */
    /* We initialize the secondary languages first so our last init is */
    /* the primary language */
    //secondary_langs = 
    //    val_readlist_string(flite_get_param_string(vox->features,
    //                                               "secondary_languages",""));
    //for (sc = secondary_langs; sc; sc=val_cdr(sc))
    //{
    //    cg_init_lang_lex(vox,lang_table,val_string(val_car(sc)));
    //}
    //delete_val(secondary_langs);
    printf("cccccca\n");
    /* Init primary language */
    lex = cg_init_lang_lex(vox,lang_table,language);

    if (lex == NULL)
    {   /* Language is not supported */
        cst_errmsg("Error load voice: lang/lex %s not supported in this binary\n",language);
	return NULL;	
    }
   //lex = net_fr_lex_init();

    /* Things that weren't filled in already. */
    flite_feat_set_string(vox->features,"name",name);
    flite_feat_set_string(vox->features,"pathname",filename);
    printf("ddddd\n");

    /* Lexicon */
    flite_feat_set(vox->features,"lexicon",lexicon_val(lex));
     printf("ddddd\n");
    val_print(stdout, uttfunc_val(lex->postlex));
    flite_feat_set(vox->features,"postlex_func",uttfunc_val(lex->postlex));
     printf("ddddd\n");

    /* No standard segment durations are needed as its done at the */
    /* HMM state level */
    flite_feat_set_string(vox->features,"no_segment_duration_model","1");
    flite_feat_set_string(vox->features,"no_f0_target_model","1");

    printf("eeeee\n");
    /* HTS engine */
    flite_hts = cst_alloc(Flite_HTS_Engine, 1);
    Flite_HTS_Engine_initialize(flite_hts);

    printf("ffffff\n");
    /* load HTS voice */
    char *fn_voice = flite_hts_get_voice_file(vox);
    printf(fn_voice);
    printf("ggggggg\n");
    if (fn_voice != NULL)
    {
        flite_feat_set_string(vox->features, "htsvoice_file", fn_voice);
        cst_free(fn_voice);
    }
    else
    {
        flite_feat_set_string(vox->features, "htsvoice_file",
                              filename);
    }
    flite_feat_set(vox->features, "flite_hts", flitehtsengine_val(flite_hts));

    printf("hhhhhh\n");
    /* Waveform synthesis */
    flite_feat_set(vox->features,"wave_synth_func",uttfunc_val(&hts_synth));
    flite_feat_set_int(vox->features,"sample_rate",44000);

    return vox;
}

void cst_hts_unload_voice(cst_voice *vox)
{
    delete_voice(vox);
}


