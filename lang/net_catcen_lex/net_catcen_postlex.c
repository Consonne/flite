/*************************************************************************/
/*                                                                       */
/*                  Language Technologies Institute                      */
/*                     Carnegie Mellon University                        */
/*                      Copyright (c) 2001-2008                          */
/*                        All Rights Reserved.                           */
/*                                                                       */
/*  Permission is hereby granted, free of charge, to use and distribute  */
/*  this software and its documentation without restriction, including   */
/*  without limitation the rights to use, copy, modify, merge, publish,  */
/*  distribute, sublicense, and/or sell copies of this work, and to      */
/*  permit persons to whom this work is furnished to do so, subject to   */
/*  the following conditions:                                            */
/*   1. The code must retain the above copyright notice, this list of    */
/*      conditions and the following disclaimer.                         */
/*   2. Any modifications must be clearly marked as such.                */
/*   3. Original authors' names are not deleted.                         */
/*   4. The authors' names are not used to endorse or promote products   */
/*      derived from this software without specific prior written        */
/*      permission.                                                      */
/*                                                                       */
/*  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         */
/*  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      */
/*  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   */
/*  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      */
/*  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    */
/*  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   */
/*  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          */
/*  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       */
/*  THIS SOFTWARE.                                                       */
/*                                                                       */
/*************************************************************************/
/*             Author:  Alan W Black (awb@cs.cmu.edu)                    */
/*               Date:  August 2008                                      */
/*************************************************************************/
/*  Moved the CMU lexicon specific postlexical rules into net_catcen_lex itself  */
/*************************************************************************/

#include "flite.h"



// M -------------//

static void liaison(cst_utterance *u)
{
    cst_item *s;
    cst_item *liaison;
    const char *pword, *word;

   int nliaison=0, zliaison=0;

   // suit la structure des exemples , mais il y a-t-il une raison pour qu'on ne boucle pas sur les mots au lieu des segments ?
    for (s=relation_head(utt_relation(u,"Segment")); 
	 s; s=item_next(s))
    {
	word = val_string(ffeature(s, "R:SylStructure.parent.parent.name"));

	// existe-til des liaison avec deux mêmes mots qui se suivent ? pourrait être problématique
        
	if (!cst_streq(pword,word) && !cst_streq(word,"0"))
        {
		if (nliaison)
        	{
           	 	liaison= item_prepend(s,NULL);
            		item_set_string(liaison,"name","n");
	    		item_prepend(item_as(s,"SylStructure"),liaison);	
		}

		if (zliaison)
        	{
            		liaison= item_prepend(s,NULL);
            		item_set_string(liaison,"name","z");
	    		item_prepend(item_as(s,"SylStructure"),liaison);      
        	}
		
        }
	nliaison =0;
	zliaison =0;



        if (cst_streq("+", ffeature_string(s,"n.ph_vc")))
        {
		if ( (item_name(s)[2] == 'n') &&(word[(strlen(word)-1)] == 'n'))  // nasales : An On En 9n qui finit par la lettre n
                {
		 	nliaison = 1;
			// liason en n sans dénasalisation : aucun, bien, en, on , rien, un, son ton, mon
        		static const char * const pc[] = { "un", "son", "ton", "mon", "on", "en", "rien", "bien", "aucun", NULL };

        	
			if (!cst_member_string(word, pc))
			{
            			item_set_string(s, "name", cst_substr(item_name(s),0,2)); // pour On garde O , An , A
				// bon appétit   bO nA
            			// je n'arrive pas à attacher la liaison au mot d'après juste au mot actuel
            			// c'est pour ca que "j'attend" d'arriver au bon endroit
            
			}

        	}
                // les,des, vous, deux, vous,elles
		else if (cst_streq("les", word)||cst_streq("des", word)||cst_streq("vous", word)||cst_streq("elles", word))
                {
			cst_errmsg(" %s is special z word \n ", word);
                      zliaison =1;
                }

        }
        pword = word; 
        
    }
}


static void liaison_tiret(cst_utterance *u)
{
    cst_item *s;
    cst_item *liaison;
    const char *pword, *word;

    for (s=relation_head(utt_relation(u,"Segment")); 
	 s; s=item_next(s))
    {
	word = val_string(ffeature(s, "R:SylStructure.parent.parent.name"));

	if ((word[0]=='-')&&(pword !=word)&&cst_streq("+", ffeature_string(s,"ph_vc")))
        {	
		
		if (pword[(strlen(pword)-1)]=='t')
		{
		liaison= item_prepend(s,NULL);
            	item_set_string(liaison,"name","t");
	    	item_prepend(item_as(s,"SylStructure"),liaison);
		}
		else if (pword[(strlen(pword)-1)]=='s')
		{
		liaison= item_prepend(s,NULL);
            	item_set_string(liaison,"name","z");
	    	item_prepend(item_as(s,"SylStructure"),liaison);
		}
	}
        pword = word; 
        
    }

}


static void show_phones(cst_utterance *u)
{
	cst_item *s;
	    for (s=relation_head(utt_relation(u,"Segment")); 
	 s; s=item_next(s))
    {
	 cst_errmsg("finally %s\n",item_name(s));
    }

}

cst_utterance *net_catcen_postlex(cst_utterance *u)
{
    /* Post lexical rules */
    show_phones(u);
    liaison(u);
    liaison_tiret(u);
	show_phones(u);

    return u;
}
