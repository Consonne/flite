/*************************************************************************/
/*                                                                       */
/*                  Language Technologies Institute                      */
/*                     Carnegie Mellon University                        */
/*                         Copyright (c) 2013                            */
/*                        All Rights Reserved.                           */
/*                                                                       */
/*  Permission is hereby granted, free of charge, to use and distribute  */
/*  this software and its documentation without restriction, including   */
/*  without limitation the rights to use, copy, modify, merge, publish,  */
/*  distribute, sublicense, and/or sell copies of this work, and to      */
/*  permit persons to whom this work is furnished to do so, subject to   */
/*  the following conditions:                                            */
/*   1. The code must retain the above copyright notice, this list of    */
/*      conditions and the following disclaimer.                         */
/*   2. Any modifications must be clearly marked as such.                */
/*   3. Original authors' names are not deleted.                         */
/*   4. The authors' names are not used to endorse or promote products   */
/*      derived from this software without specific prior written        */
/*      permission.                                                      */
/*                                                                       */
/*  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         */
/*  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      */
/*  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   */
/*  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      */
/*  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    */
/*  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   */
/*  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          */
/*  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       */
/*  THIS SOFTWARE.                                                       */
/*                                                                       */
/*************************************************************************/
/*                                                                       */
/*  swahili Lexical function                                            */
/*                                                                       */
/*************************************************************************/
#include "flite.h"
#include "cst_val.h"
#include "cst_voice.h"
#include "cst_lexicon.h"
#include "cst_ffeatures.h"
#include "net_swa_lex.h"
#include <stdio.h>
#include <ctype.h>



#define ALLOW_NONSTANARD_PHONEMES (FALSE)

cst_val *net_swa_lex_lts_function(const struct lexicon_struct *l, 
                                   const char *word, const char *pos,
                                   const cst_features *feats)
{
    //printf("cmu_grapheme_lex.c: word \"%s\" \n",word);
    cst_val *phones = 0;
    int i;
    const int len=strlen(word);
	//char consonantDoesntNeedSyllable=TRUE;
	int syllableCount=0;
	char isLast=TRUE;
	
	//Diphones between words still sound bad. Add pauses.
	if(ALLOW_NONSTANARD_PHONEMES)
	{
		phones = cons_val(string_val("spau"),
								  phones);
	}
	//phones = cons_val(string_val("pau"),
	//				  phones);
	for (i = len-1; i >=0; i--){
	    char* nextToken=NULL;
	    const char c=tolower(word[i]);
	    char charAfter = i < len-1?tolower(word[i+1]):'a'; //need to insert mini filler "uw"
		char isOK=FALSE;
		switch(c)
		{
			case 'a': case 'e': case 'i': case 'o': case 'u':
				isOK=TRUE;
				syllableCount++;
				break;
			case 'm':
				switch(charAfter){
					case 'a': case 'e': case 'i': case 'o': case 'u': case 'b':
						isOK=TRUE;
						break;
					default:
						isOK=FALSE;
				}
				break;
			case 'n':
				switch(charAfter){
					case 'a': case 'e': case 'i': case 'o': case 'u': case 'd': case 'g': case 'j': case 'y':
						isOK=TRUE;
						break;
					default:
						isOK=FALSE;
				}
				break;
				
				//Missing "break" is deliberate... I want to the code to execute the next case as well.
				break;
			default:
				switch(charAfter){
					case 'a': case 'e': case 'i': case 'o': case 'u':
						isOK=TRUE;
						break;
					default:
						isOK=FALSE;
				}
				break;
			
		}
		if(ALLOW_NONSTANARD_PHONEMES && !isOK){
	    	phones = cons_val(string_val("uwmini"),
							  phones);
		}
		switch(c)
		{
			case '\'': //Handle ng'
				if(i>1 && tolower(word[i-1])=='g'  && tolower(word[i-2])=='n'){ //it's ng'
		    		//consonantDoesntNeedSyllable=TRUE;
		    		i-=2;
		    		nextToken="ng";
				}
				break;
			case 'a':
				if(ALLOW_NONSTANARD_PHONEMES && syllableCount==2)
					nextToken="a1";
				else if(ALLOW_NONSTANARD_PHONEMES && isLast)
					nextToken="a2";
				else if(syllableCount==2)
					nextToken="a1";
				else
					nextToken="a";
				break;
			case 'b':
				nextToken="b";
				break;
			case 'c':
				nextToken="ch";
				//TODO: handle "CH"
				break;
			case 'd':
				nextToken="d";
				//TODO: handle "DH"
				break;
			case 'e':
				if(ALLOW_NONSTANARD_PHONEMES && syllableCount==2)
					nextToken="e1";
				else if(ALLOW_NONSTANARD_PHONEMES && isLast)
					nextToken="e2";
				else if(ALLOW_NONSTANARD_PHONEMES && syllableCount==2)
					nextToken="e1";
				else
					nextToken="e";
				break;
			case 'f':
				nextToken="f";
				break;
			case 'g':
				nextToken="g";
				if(ALLOW_NONSTANARD_PHONEMES && i>0 && tolower(word[i-1])=='n'){ //it's ng'
		    		//consonantDoesntNeedSyllable=TRUE;
		    		i-=1;
		    		nextToken="ng1";
				}
				break;
			case 'h':
				nextToken="h";
				if(i>0) //Handle digraphs like "th"
				{
					const char c2=tolower(word[i-1]);
					switch(c2){
						case 'c': //chai
							nextToken="ch";
							i--;
							break;
						case 'd': //dhabi
							nextToken="dh";
							i--;
							break;
						case 'g': //ghana
							nextToken="gh"; //Missing from Arpabet, should be gh 
							i--;
							break;
						case 'k':
							nextToken="kh"; //Missing from Arpabet, should be as in loch 
							i--;
							break;
						case 's':
							nextToken="sh";
							i--;
							break;
						case 't':
							nextToken="th";
							i--;
							break;
					}
				}
				break;
			case 'i':
				if(ALLOW_NONSTANARD_PHONEMES && syllableCount==2)
					nextToken="i1";
				else if(ALLOW_NONSTANARD_PHONEMES && isLast)
					nextToken="i2";
				else if(syllableCount==2)
					nextToken="i1";
				else
					nextToken="i";
				break;
			case 'j':
				nextToken="j";
				break;
			case 'k':
				nextToken="k";
				break;
			case 'l':
				nextToken="l";
				break;
			case 'm':
				if(ALLOW_NONSTANARD_PHONEMES && syllableCount==2)
					nextToken="m2";
				else if(syllableCount==2)
					nextToken="m1";
				else
					nextToken="m";
				break;
			case 'n':
				if(ALLOW_NONSTANARD_PHONEMES && syllableCount==2)
					nextToken="n2";
				else if(syllableCount==2)
					nextToken="n1";
				else
					nextToken="n";
				break;
			case 'o':
				if(ALLOW_NONSTANARD_PHONEMES && syllableCount==2)
					nextToken="a1";
				else if(ALLOW_NONSTANARD_PHONEMES && isLast)
					nextToken="a2";
				else if(syllableCount==2)
					nextToken="a1";
				else
					nextToken="a";
				break;
			case 'p':
				nextToken="p";
				break;
			case 'r':
				nextToken="r";
				break;
			case 's':
				nextToken="s";
				break;
			case 't':
				nextToken="t";
				break;
			case 'u':
				if(ALLOW_NONSTANARD_PHONEMES && syllableCount==2)
					nextToken="u1";
				else if(ALLOW_NONSTANARD_PHONEMES && isLast)
					nextToken="u2";
				else if(ALLOW_NONSTANARD_PHONEMES && syllableCount==2)
					nextToken="u1";
				else
					nextToken="u";
				break;
			case 'v':
				nextToken="v";
				break;
			case 'w':
				nextToken="w";
				break;
			case 'y':
				nextToken="y";
				if(ALLOW_NONSTANARD_PHONEMES && i>0 && tolower(word[i-1])=='n'){ //it's ng'
		    		//consonantDoesntNeedSyllable=TRUE;
		    		i-=1;
		    		nextToken="ny";
				}
				break;
			case 'z':
				nextToken="z";
				break;
		}
		isLast=FALSE;
		
    	//do_things_with(c);
                //phones = cons_val(string_val("hh"),
                //                  phones);
        if(nextToken)
			phones = cons_val(string_val(nextToken),
							  phones);
	}
	
    //phones = val_reverse(phones);
    val_print(stdout,phones);
    printf("\n");
    return phones;
}



cst_utterance *net_swa_postlex(cst_utterance *u)
{
    //printf("cmu_grapheme_lex.c: postlex\n");
//    val_print(stdout,u);
//    printf("\n");
    return u;
}

int net_swa_syl_boundary_mo(const cst_item *i,const cst_val *rest)
{
    return TRUE;
    /* syl boundary maximal onset */
    int d2v;

    if (rest == NULL)
	return TRUE;
    else if (net_is_silence(val_string(val_car(rest))))
	return TRUE;
    else if (!net_has_vowel_in_list(rest)) 
        /* no more vowels so rest *all* coda */
	return FALSE;
    else if (!net_has_vowel_in_syl(i))  /* need a vowel */
        /* no vowel yet in syl so keep copying */
	return FALSE;
    else if (net_is_vowel(val_string(val_car(rest))))
        /* next is a vowel, syl has vowel, so this is a break */
	return TRUE;
    else if (cst_streq("ng",val_string(val_car(rest))))
        /* next is "ng" which can't start a word internal syl */
	return FALSE;
    else 
    {
        /* want to know if from rest to the next vowel is a valid onset */
        d2v = net_de_lex_dist_to_vowel(rest);
        if (d2v < 2)
            return TRUE;
        else if (d2v > 3)
            return FALSE;
        else if (d2v == 2) 
            return net_de_lex_onset_bigram(rest);
        else /* if (d2v == 3) */
            return net_de_lex_onset_trigram(rest);
        return TRUE;
    }

}

cst_lexicon *net_swa_lex_init(void)
{
    /* Should it be global const or dynamic */
    /* Can make lts_rules just a cart tree like others */
    cst_lexicon *l;

    l = cst_alloc(cst_lexicon,1);
    l->name = "net_swa_lex";

    l->lts_function = net_swa_lex_lts_function;
    l->syl_boundary = net_swa_syl_boundary_mo;
    
    l->postlex = net_swa_postlex;

    return l;

}
