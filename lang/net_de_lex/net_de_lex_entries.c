/*******************************************************/
/**  generated lexicon entry file from net_de    */
/*******************************************************/

#include "cst_string.h"
#include "cst_lexicon.h"

const int net_de_lex_num_bytes = 
#include "net_de_lex_num_bytes.c"
;

const int net_de_lex_num_entries = 14059;

const char * const net_de_lex_phone_table[86] = 
{
    "_epsilon_",
    "p",
    "a1",
    "GS",
    "aU0",
    "b",
    "n",
    "schwa",
    "m",
    "O0",
    "k",
    "N",
    "U0",
    "d",
    "l",
    "I0",
    "E0",
    "t",
    "ah1",
    "s",
    "h",
    "ih60",
    "Z",
    "a0",
    "g",
    "Ahn0",
    "P6",
    "OY0",
    "ah0",
    "oh1",
    "j",
    "r",
    "E60",
    "ah60",
    "f",
    "eh0",
    "z",
    "ch",
    "Y",
    "P2h6",
    "ih0",
    "aI0",
    "O60",
    "C",
    "S",
    "Y6",
    "v",
    "oh0",
    "Eh60",
    "uh0",
    "uh61",
    "yh",
    "P2h",
    "Ahn1",
    "yh6",
    "uh1",
    "E1",
    "ih61",
    "O1",
    "uh60",
    "U60",
    "U61",
    "eh1",
    "aI1",
    "ih1",
    "I60",
    "Eh61",
    "OY1",
    "O61",
    "Ohn0",
    "I1",
    "E61",
    "Ohn1",
    "a60",
    "oh60",
    "P96",
    "a61",
    "ah61",
    "aU1",
    "P9",
    "U1",
    "oh61",
    "P3hn",
    "I61",
    "P9hn",
    NULL
};

const char * const net_de_lex_phones_huff_table[257] = 
{
    NULL, /* reserved */
#include "net_de_lex_phones_huff_table.c"
    NULL
};

const char * const net_de_lex_entries_huff_table[257] = 
{
    NULL, /* reserved */
#include "net_de_lex_entries_huff_table.c"
    NULL
};

